I have never used GIT before, I mean when I was a student. But I recently have to work with big projects, using Git is necessary.
Currently, I work at an Outsourcing Company, using GIT isn't necessary that is important! < MUST >

OK, LET'S GO!
-
    * I define some rules to write my diary well.
        -   I put all git command bettwen "bouble quote" . I example : "git push"

    * Reference
        - I le : https://bitbucket.org/designveloper/leduci_git/src
        - Lynda : https://www.lynda.com/Git-tutorials

I. INTRODUCE & INSTALL GIT ON YOUR MACHINE

* GIT ???

    Git is sofware that keeps track of changes that you make to file and directories and it is especially good in keeping track of text  changes that you make.

1. Install & Set up Git
    -  Go to https://git-scm.com/
    -   Download & install.

        -   After download & install successful.  Opend Git and type :
            
               Command:
                            
                 "which git" (this command will show you where Git is installed and located)

                 Command:
                 
                 "git --version" (this command will show you the version currently)
                    (".msysgit" is letting us know that's the windown version).

        -  Configuring Git
        
                Before configuration, You need to know where is three places that Git stores.
                    .   System.
                        +    /etc/gitconfig  
                            ProgramFiles\Git\etc\gitconfig

                            *** command :
                            "git config --system" 
                    .   User.
                        +    ~/.gitconfig  
                            $HOME\.gitconfig

                            *** command :
                            "git config --global" 
                    -   Project  
                        +   my_project/.git/config 

                            *** command :
                            "git config"
         -   Git help

                -       command: 
                            "git help <command>"
                                f to forward, b for backward
                                man git-<command>

                                
2. Set up a repository
    -
                command :
                
                "git init" to initializing a repository
                    
                *** How to commit:  

                command:
                    "git add . " to add all changes before
                    "git commit -m "<message>" " to add message for commit
                    "git commit -am "<message>""

                or            
                    "git log " - To show all commit before
                    "git log -n 1 ": watch commit by order
                    "git log --since " : watch commit by date  
                    "git long --until ": watch commit until date

II. WORKING ON GIT
-
1. Git concept & Architecture
    - To understand how Git works

             Two-tree architecture 
                repository  <==> working
                working <==> commit <==> repository
                repository <==> checkout <==> working

            Three-tree architecture
                working <==> staging index <==> repository
                working <==> git add <==> staging index  
                staging index <==> git commit <==> repository  
                repository <==> checkout <==> working

2. Making Changes to Files
    - To add files 

                command :
                    " git status" to get know what changed.
                    " git add ." to add all of changed files
                    " git commit -m "message""  or "git commit -am "message"" 

    - To see files changes by diff 

                " git diff " to see whole changes between all files in working directory  
                " git diff filename.ext" to see only changes within specific file.  
                " git diff banchname1 branchname2 " to see differences between two branches

    - To see only staged changes

                " git diff --staged/--cache " to watch changes in staging index

    -   To delete files

            Manual. 
            By command: 
                "git rm filename"

    - To Move or Rename files
        - Rename file:  

                git mv file1 file2 
        - Moving files:

                git mv file1 folder/file2


3. Undo changes
    
    commnad :

        "git chekout -- file"
        "git reset HEAD file"
        "git commit -amend -m "message"" to changes message last commit
4. Ignore

    Using .gitignore

    - Negate expressions with

            *.php <==> ignore all file with php extension
            !index.php  <==> except index.php

5. Branch

    command: 

    - To see branch where you standing
            
            "git branch"
    - To see all branch

            "git branch -a"
    - To create new branch

            "git branch new_branchname"

            or 
            Create and switch branch
            "got checkout -b new_branchname
    - To switch branch

            "git checkout branch_need_to_switch""

             or 
                 Create and switch branch
                 "got checkout -b new_branchname
    - To compare branches

            "git diff" to see all different

            then
            "git --merge branch_need_to_merge"

    - To Rename / Delete branch
        - To rename

                 "git branch -m name_of_new_branch"
        - To delete
                
                "git branch -d branch_want-to-delete"
                or
                "git branch -D branch_want_to_delete"  To force to delete
    - To merge branch
        - command 

                "git checkout branch_want_to_merge"
                then
                "git merge branch_need_to_merge"
        - Default Merge is fast-forward (ff)
            - Beside also there is true merge 

                    "git merge --no-ff branch_need_to_merge"
                    "git merge --ff-only branch_need_to_merge"
        - Merging conflicts & work out

                    "git merge --abort"

                     * workout

                    keep lines short  
                    keep commits small and focused  
                    beware stray edits to whitespace  
                    spaces, tabs, line returns  
                    merge often  
                    track changes to master  

